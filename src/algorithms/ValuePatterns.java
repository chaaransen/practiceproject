package algorithms;

import java.util.Scanner;

public class ValuePatterns {

	public static void main(String[] args) {
		System.out.println("Enter the no");
		Scanner s = new Scanner(System.in);
		int value = s.nextInt();
		int MAX_VAL = 8;
		int MAX_DAYS = 7;
		for (int i = 2; i < MAX_VAL; i++) {
			int remHours = value;
			while (remHours > 0) {
				remHours -= i;
				if (remHours < 0) {
					int val = remHours + i;
					System.out.print(val + " ");
				} else {
					System.out.print(i + " ");
				}
			}
			System.out.println("");
		}
	}

}
