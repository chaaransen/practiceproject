package algorithms;

import java.util.Scanner;

public class Permutations {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		char[] input = s.nextLine().toCharArray();
		permutate(input, 0, input.length - 1);
	}

	public static void permutate(char[] input, int left, int right) {
		if (left == right) {
			System.out.println(input);
		} else {
			for (int i = left; i <= right; i++) {
				input = swap(input, left, i);
				permutate(input, left + 1, right);
				input = swap(input, left, i);
			}
		}

	}

	public static char[] swap(char[] word, int a, int b) {
		char temp;
		temp = word[a];
		word[a] = word[b];
		word[b] = temp;
		return word;
	}
}
