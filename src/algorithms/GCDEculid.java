package algorithms;

import java.util.Arrays;
import java.util.Scanner;

public class GCDEculid {
	public static void main(String[] args) {
		System.out.println("Enter 2 nos");
		Scanner s = new Scanner(System.in);
		String nos = s.nextLine();
		int[] no = Arrays.stream(nos.split(" ")).mapToInt(Integer::parseInt).toArray();

		int gcd = findGCD(no[0], no[1]);
		// // Without recursion
		// int remainder = 1;
		// int dividend;
		// int divisor;
		// int gcd = 0;
		// if (no[0] > no[1]) {
		// dividend = no[0];
		// divisor = no[1];
		// } else {
		// dividend = no[1];
		// divisor = no[0];
		// }
		// while (remainder != 0) {
		// remainder = dividend % divisor;
		// if (remainder == 0) {
		// gcd = divisor;
		// break;
		// } else {
		// dividend = divisor;
		// divisor = remainder;
		// }
		//
		// }
		System.out.println("GCD : " + gcd);
	}

	public static int findGCD(int a, int b) {
		if (b == 0) {
			return a;
		}
		return findGCD(b, a % b);
	}
}
