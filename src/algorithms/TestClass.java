package algorithms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class TestClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		String raw1 = s.nextLine(); // Reading input from STDIN

		// length of array / queries
		int[] input = Arrays.stream(raw1.split(" ")).mapToInt(Integer::parseInt).toArray();
		String raw2 = s.nextLine(); // Reading input from STDIN
		// test array
		int[] nos = Arrays.stream(raw2.split(" ")).mapToInt(Integer::parseInt).sorted().toArray();

		for (int i = 0; i <= input[1]; i++) {
			int no = s.nextInt();
			no++;
			if (no > nos[input[0] - 1]) {
				System.out.println(no);
			} else if (no < nos[0]) {
				System.out.println(no);
			} else {
				for (int j = 0; j < input[0]; j++) {
					if (no > nos[j]) {
						continue;
					} else if (no < nos[j]) {
						System.out.println(no);
						break;
					} else if (no == nos[j]) {
						no++;
						if (j == input[0] - 1) {
							System.out.println(no);
						}

						continue;
					}
				}
			}
		}
	}

}
