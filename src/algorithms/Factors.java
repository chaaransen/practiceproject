package algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Factors {
	public static void main(String args[]) {

		int x = 1;
		while (x == 1) {
			List<Integer> factors = new ArrayList<Integer>();
			Scanner s = new Scanner(System.in);
			int inputNo = s.nextInt();
			int sqrtNo = (int) Math.sqrt(inputNo);
			for (int i = 1; i <= sqrtNo; i++) {
				if (inputNo % i == 0) {
					int coFactor = inputNo / i;
					if (i != coFactor) {
						factors.add(i);
					}
					factors.add(coFactor);
				}
			}
			factors = factors.stream().sorted().collect(Collectors.toList());
			factors.forEach((factor) -> System.out.print(factor + " "));
			s.nextLine();
			x = s.nextInt();
		}

	}
}
