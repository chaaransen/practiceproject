package algorithms;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class PrimeOrNot {
	public static void main(String args[]) {
		int x = 1;
		while (x == 1) {
			Scanner s = new Scanner(System.in);
			int inputNo = s.nextInt();
			// System.out.println(inputNo);
			boolean primeNo = true;
			int sqrtNo = (int) Math.sqrt(inputNo);
			// System.out.println(sqrtNo);
			for (int i = 2; i <= sqrtNo; i++) {
				if (inputNo % i == 0) {
					primeNo = false;
					break;
				}
			}
			if (primeNo) {
				System.out.println("Prime No");
			} else {
				System.out.println("Not a prime No");
			}
			s.nextLine();
			x = s.nextInt();
		}

	}
}
