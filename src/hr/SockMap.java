package hr;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class SockMap {
	public static void main(String[] args) {
		int rawData[] = { 1, 2, 2, 1, 1, 1, 2, 1, 3, 3, 3 };
		int pairs = 0;

		Map<Integer, Integer> sockColors = new HashMap<Integer, Integer>();
		for (int x : rawData) {
			if (sockColors.containsKey(x)) {
				sockColors.put(x, sockColors.get(x) + 1);
			} else {

				sockColors.put(x, 1);
			}
		}

		for (Entry<Integer, Integer> e : sockColors.entrySet()) {
			pairs += e.getValue() / 2;
		}

		System.out.println("Total pairs " + pairs);
	}
}
