package testPackage;

public class ThreadLocalExample {

	public static class MyRunnable implements Runnable {

		private ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>();
		// int x = 0;

		@Override
		public void run() {
			System.out.println(threadLocal.get());
			threadLocal.set((int) (Math.random() * 100D));

			// System.out.println("Before " + x);
			// x = (int) (Math.random() * 100D);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

			System.out.println(threadLocal.get());
			// System.out.println(x);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		MyRunnable sharedRunnableInstance = new MyRunnable();

		Thread thread1 = new Thread(sharedRunnableInstance);
		Thread thread2 = new Thread(sharedRunnableInstance);

		thread1.start();
		thread1.join(); // wait for thread 1 to terminate
		thread2.start();

		thread2.join(); // wait for thread 2 to terminate
	}

}