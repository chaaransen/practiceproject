package testPackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class CommonForkJoinPoolExample1 {

	public static void main(String args[]) throws InterruptedException {
		final List<Integer> numbers = getNumbers();
		numbers.parallelStream().forEach(n -> {
			try {
				System.out.println("Before " + n + " " + Thread.currentThread());
				Thread.sleep(5);
				System.out.println("After " + n + " " + Thread.currentThread());
				System.out.println("");
			} catch (InterruptedException e) {
			}
		});
	}

	private static List<Integer> getNumbers() {
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		for (int i = 0; i < 5; i++)
			numbers.add(i);
		return Collections.unmodifiableList(numbers);
	}
}
