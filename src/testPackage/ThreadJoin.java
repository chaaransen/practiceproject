package testPackage;

import java.util.Date;

public class ThreadJoin {
	public static void main(String[] args) {
		System.out.println("Main Thread " + Thread.currentThread() + "\n");
		Thread t1 = new Thread(() -> {
			System.out.println("Thread 1 running... " + Thread.currentThread());
			// Thread t12 = new Thread(() -> System.out.println(Thread.currentThread()));
			// t12.start();
			
//			System.out.println("Thread 1 running...COMPLETED!" + Thread.currentThread());
			
			try {
				System.out.println("Thread 1 sleeping");
				Thread.sleep(2000);
				System.out.println("Thread 1 running...COMPLETED!" + Thread.currentThread());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		Thread t2 = new Thread(() -> {
			System.out.println("Thread 2 running...COMPLETED!" + Thread.currentThread());
		});

		t1.start();
		System.out.println("T1 started ");
		
		
		t2.start();
		System.out.println("T2 started ");
		
		try {
			System.out.println("T1 Join " + Thread.currentThread());
			t1.join();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// System.out.println("Join T1 Expired " + Thread.currentThread());
		// System.out.println("\nT2 started ");

		// try {
		// System.out.println("T2 Join " + date.getTime());
		// t2.join(5000);
		//
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}
}
