package skillenza;

import java.util.Arrays;
import java.util.Scanner;

public class solution {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int upperLimit = (int) Math.pow(10, 5);
		int testCases = s.nextInt();
		if (testCases <= 1000 && testCases >= 1) {
			for (int t = 0; t < testCases; t++) {
				int days = s.nextInt();
				// System.out.println(days);
				if (days >= 1 && days <= upperLimit)
					s.nextLine();
				String kmsRaw = s.nextLine();
				String[] kms = kmsRaw.split(" ");
				int[] km = Arrays.stream(kms).mapToInt(Integer::parseInt).sorted().toArray();
				int totCals = 0;
				int kmsRun = 0;
				for (int i = km.length - 1; i >= 0; i--) {
					// System.out.print("2 * " + kmsRun + " +" + km[i] + " = ");
					if (km[i] >= 1 && km[i] <= upperLimit) {
						totCals += (kmsRun * 2) + km[i];
						kmsRun += km[i];
					} else {
						continue;
					}
					// System.out.println(totCals);

				}
				System.out.println(totCals);
			}
		}

	}

}
